# -*- coding: utf-8 -*-
import paho.mqtt.client as mqtt
import sys
import time
import json
#import spidev

#conectar arduino com DragonBoard
import serial
ard = serial.Serial('/dev/tty96B0', 9600)
print("import")
# Bluemix
#import ibmiotf.application

# Dragonboard
from time import sleep

#spi = spidev.SpiDev()
#spi.open(0,0)
#spi.max_speed_hz=10000
#spi.mode = 0b00
#spi.bits_per_word = 8
#channel_select=[0x01, 0xA0, 0x00]

#if sys.version_info[0] == 3:
#    input_func = input
#else:
#    input_func = raw_input

#Set the variables for connecting to the iot service
broker = ""
topic = "iot-2/evt/status/fmt/json"
username = "use-token-auth"
password = "D1C07mjhB4N!qVFvp9" #auth-token
organization = "rydhlu" #org_id
deviceType = "Temperatura"
deviceId = "dragonboard410cID"

topic = "iot-2/evt/status/fmt/json"

#Creating the client connection
#Set clientID and broker
clientID = "d:" + organization + ":" + deviceType + ":" + deviceId
broker = organization + ".messaging.internetofthings.ibmcloud.com"
mqttc = mqtt.Client(clientID)
print ("mqtt")
#Set authentication values, if connecting to registered service
if username is not "":
    mqttc.username_pw_set(username, password=password)
print ("mqtt2")
mqttc.connect(host=broker, port=1883, keepalive=60)
print ("mqtt3")

def showTemp(humid, temp):
  humid = json.JSONEncoder().encode({"d":{"humidade":humid}})
  mqttc.publish(topic, payload=humid, qos=0, retain=False)

  temp = json.JSONEncoder().encode({"d":{"temperatura":temp}})
  mqttc.publish(topic, payload=temp, qos=0, retain=False)

if __name__=='__main__':
    print("Welcome to the Humidity & Temperature reader!!!")
    mqttc.loop_start()
    while mqttc.loop() == 0:

      ardOut = ard.readline()
      if ardOut.find(":") != -1:
          #ardHumid = ardOut.split('Temperature:')[0]
          #ardTemp = ardOut.split('Temperature:')[1]
          dados = ardOut.split(':')
          print("humidade " + dados[0])
          print("temp: " + dados[1])
          showTemp(dados[0],dados[1])
      time.sleep(5)
      pass
